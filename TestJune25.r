########### Test R June 25,2018 ###########
###########################################

#Q1#

#A.read the DB
df <- read.csv("df.csv", stringsAsFactors = FALSE)

#B.check if theres attribue that will need to change to factor
str(df)
#yes there are - will change credit.policy, not.fully.paid, and pub.rec to factor
df$credit.policy <- as.factor(df$credit.policy)
df$not.fully.paid <- as.factor(df$not.fully.paid) #0-fully paid, 1-not fully paid
df$pub.rec <- as.factor(df$pub.rec)

#C.Check if theres NA values, if so, change them
any(is.na(df)) #False, theres no NA

#Q2# 
install.packages('ggplot2')
library(ggplot2)

# A.Predict the influence of int.rate on not.fully.paid
p1 <- ggplot(df, aes(x=df$int.rate, fill = df$not.fully.paid))
p1 <- p1 + geom_histogram(color= 'green')
p1 <- p1 + ggtitle('influence of int.rate on not.fully.paid')

#conclusion is interest rate 0.10-0.15 have a higher chance of return

# B.Predict the influence of purpose on not.fully.paid
p2 <- ggplot(df, aes(x=purpose))
p2 <- p2 + geom_bar(color= 'green', fill= 'light green')
#second layer & parameter
p2 <- p2 + geom_bar(aes(fill = not.fully.paid))
p2 <- p2 + geom_bar(aes(fill = not.fully.paid), position = 'fill')
p2 <- p2 + ggtitle('influence of purpose on not.fully.paid')

#conclusion is the main purpose for taking a loan is for debt consolidation

# C.Predict the influence of fico to int.rate
p3 <- ggplot(df, aes(x=fico, y = int.rate))
p3 <- p3 + geom_point(alpha=0.2)
p3 <- p3 + ggtitle('influence of fico to int.rate')
#conclusion is that people with lower fico score tend to get higher interest rate

# D.we would like to ues credit.policy as a singal prediction to installment, what will be the recall& his precision?

df$credit.policy # ZERO OR ONE 0/1
#recall <- TP/(TP+FN)
#precision <- TP/(TP+FP)
#TP will be zero

#Q3#

# A. dividing the df to train set and test set.
dim(df)

rows <- nrow(df)
split <- runif(rows)
split <- split >= 0.3

df.train <- df[split,] 
df.test <- df[!split,]

nrow(df.test)
nrow(df.train)

# B.Desicion tree
install.packages('ISLR')
library(ISLR)

install.packages('rpart')
library(rpart)

install.packages('rpart.plot')
library(rpart.plot)

install.packages('randomForest')
library(randomForest)

install.packages('caTools')
library(caTools)

sample <- sample.split(df,0.7)

train <- df[sample == T,]
test <- df[sample == F,]

#Derive the tree model 
tree <- rpart(not.fully.paid ~ .,method = 'class' ,data = train)
prp(tree) # No tree was produce.

# C.check the tree, conclusions
# we didnt get any tree because probably the attribute is worthless, and cant help to predicted.


#Q4#

# A.Logistic Regression
model <- glm(not.fully.paid ~ ., family = binomial(link = 'logit'), data = train)

summary(model)
step.model <- step(model)

summary(step.model)

predictecProb <- predict(model, newdata = test, type = 'response')

#### Error in model.frame.default(Terms, newdata, na.action = na.action, xlev = object$xlevels) : 
####factor pub.rec has new levels 4

# B.
newdf <- df
str(newdf)
newdf$pub.rec <- NULL

sample <- sample.split(newdf,0.7)

train.new <- df[sample == T,]
test.new <- df[sample == F,]

model.new <- glm(not.fully.paid ~ ., family = binomial(link = 'logit'), data = train.new)

summary(model.new)

predictecProb <- predict(model.new, newdata = test.new, type = 'response')

predictedValues <- ifelse(predictecProb > 0.5, 1, 0)


# C.creat CM
cm <- table(test.new$not.fully.paid, predictecProb>0.5)

#cm
#FALSE TRUE
#0  2433   15
#1   485   14

# D.Build a ROC curve
install.packages('pROC')
library(pROC)

rocCurve <- roc(test.new$not.fully.paid, predictecProb, levels = c("0","1"))
plot(rocCurve, col = "green", main = "Roc Chart")

# E.Maybe we should use another cut-off?
#explanation in the word file.

# # # # # END OF TEST =) # # # # #
##################################
